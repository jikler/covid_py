def comboSelect(event):
    df = pd.read_sql("SELECT * FROM users", conn)
    # Значения в комбо хранятся в строковом виде, поэтому сравнивать нужно со строкой
    idNum.configure(text = df.loc[combo.current(), 'id'])
    sexRes.configure(text = df.loc[combo.current(), 'sex'])
    birthdayRes.configure(text = df.loc[combo.current(), 'birthday'])

    # высчитываем возраст начало
    #asking the user to input their birthdate
    birthDate = df.loc[combo.current(), 'birthday']
    birthDate = datetime.datetime.strptime(birthDate, "%d.%m.%Y").date()
    # print("Your birthday is on "+ birthDate.strftime("%d") + " of " + birthDate.strftime("%B, %Y"))

    currentDate = datetime.datetime.today().date()

    #some calculations here 
    ageCalc = currentDate.year - birthDate.year
    monthVeri = currentDate.month - birthDate.month
    dateVeri = currentDate.day - birthDate.day

    #Type conversion here
    ageCalc = int(ageCalc)
    monthVeri = int(monthVeri)
    dateVeri = int(dateVeri)

    # some decisions
    if monthVeri < 0 :
        ageCalc = ageCalc-1
    elif dateVeri < 0 and monthVeri == 0:
        ageCalc = ageCalc-1

    #lets print the age now
    # print("Your age is {0:d}".format(ageCalc))
    ageRes.configure(text = format(ageCalc))
    # высчитываем возраст конец

    # print(df.loc[combo.current(), 'id'])
    print(combo.current())

# def callback1(event):
#     idNum.configure(text = valueId)


# def callback2(event):
#     idNum.configure(text = df.loc[combo.current(), 'id'])

def createDoc():
    doc.render(context)
    doc.save("Справка.docx")
    # print(combo['values'])
    # messagebox.showinfo('COVID GENERATOR 69', 'Справка на рабочем столе!')